Copyright Arnab Chakraborty 2020
# GlassesForBlind
This code works with prototype editions of Glasses which Would help people see. Uses Python 3, OpenCV, and OpenVINO, with the Intel Movidius Neural Compute Stick 2, and code created by Adrian at PyImageSearch. Raspbery Pi and NCS2 Stick Required for functionality for Image detection, as well as a Raspberry Pi Camera. Works best with Raspberry Pi 4 and beyond, with a requried 4GB of ram for optimal and fast responses.
